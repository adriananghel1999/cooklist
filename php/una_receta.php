<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// check for post data
if (isset($_GET["id"])) {
    $id = $_GET['id'];

    // get a product from products table
    $result = mysqli_query($db->connect(), "SELECT * FROM recetas WHERE id = '$id'");

    if (!empty($result)) {
        // check for empty result
        if (mysqli_num_rows($result) > 0) {

            $result = mysqli_fetch_array($result);

            $receta = array();
			$receta["id"] = $result["id"];
        
			$id_autor = $result["id_autor"];
			$categoria_id = $result["categoria_id"];
        
			$receta["id_autor"] = $id_autor;
			$receta["categoria_id"] = $categoria_id;
        
			$autor = mysqli_query($db->connect(), "SELECT `nombre` FROM `usuarios` WHERE `id`= '$id_autor'");
			$categoria = mysqli_query($db->connect(), "SELECT `nombre` FROM `categorias` WHERE `id`= '$categoria_id'");
        
			$fila = mysqli_fetch_array($autor);
			$receta["autor"] = $fila["nombre"];
			$fila = mysqli_fetch_array($categoria);
			$receta["categoria"] = $fila["nombre"];
        
			$receta["nombre"] = $result["nombre"];
			$receta["ingredientes"] = $result["ingredientes"];
			$receta["pasos"] = $result["pasos"];
			$receta["imagen"] = $result["imagen"];
            // success
            $response["success"] = 1;

            // user node
            $response["receta"] = array();

            array_push($response["receta"], $receta);

            // echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No receta found";

            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No receta found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
