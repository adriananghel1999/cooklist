-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-02-2019 a las 23:18:16
-- Versión del servidor: 5.5.60-MariaDB
-- Versión de PHP: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aga_cooklist`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE IF NOT EXISTS `recetas` (
  `id` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ingredientes` text COLLATE utf8_spanish_ci NOT NULL,
  `pasos` text COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `id_autor`, `nombre`, `ingredientes`, `pasos`, `imagen`) VALUES
(1, 1, 'Salmon With Creamy Cucumber-Fennel Salad', '4 6-ounce pieces skinless salmon fillet\n1 tablespoon olive oil\n1/4 teaspoon cayenne pepper kosher salt and black pepper\n1 lemon, thinly sliced 1/2 cup plain low-fat Greek yogurt\n1 tablespoon cider vinegar\n1 fennel bulb\ncored and thinly sliced plus 2 tablespoons chopped fennel fronds\n1/2 English cucumber\nhinly sliced rye bread for serving', 'Step 1\nHeat oven to 400 F. Rub the salmon with the oil and place on a rimmed baking sheet. Season with the cayenne and 1/4 teaspoon each salt and black pepper. Top with the lemon slices and bake until opaque throughout, 8 to 12 minutes.\nStep 2\nMeanwhile, whisk together the yogurt, vinegar, and 1/4 teaspoon each salt and black pepper in a large bowl. Add the fennel, fennel fronds, and cucumber and toss to coat.\nStep 3\nServe the salmon with the cucumber-fennel salad and bread.', 'salmon_with_creamy_cucumber_fennel_salad.jpg'),
(4, 1, 'Shrimp With Tomatoes and Olives', '1 10-ounce box couscous (1 1/3 cups)\n1 tablespoon olive oil\n1 small onion, chopped\n1 28-ounce can diced tomatoes drained\n3/4 cup pitted green olives\n1/2 cup dry white wine (such as Sauvignon Blanc) kosher salt and pepper\n1 pound medium shrimp, peeled and deveined', 'Step 1\nCook the couscous according to the package directions.\nStep 2\nMeanwhile, heat the oil in a large saucepan over medium-high heat. Add the onion and cook, stirring occasionally, for 4 minutes.\nStep 3\nAdd the tomatoes, olives, wine, 1/2 teaspoon salt, and 1/4 teaspoon pepper. Simmer, stirring occasionally, until slightly thickened, 4 to 6 minutes.\nStep 4\nAdd the shrimp and cover. Cook until the shrimp are cooked through, 3 to 5 minutes. Serve with the couscous.', 'shrimp_with_tomatoes_and_olives.jpg'),
(5, 2, 'Pancakes', '1 1/2 cups all-purpose flour\n3 tablespoons sugar\n1 tablespoon baking powder\n1/4 teaspoon salt\n1/8 teaspoon freshly ground nutmeg\n2 large eggs, at room temperature\n1 1/4 cups milk, at room temperature\n1/2 teaspoon pure vanilla extract\n3 tablespoons unsalted butter, plus more as needed', 'In a large bowl, whisk together the flour, sugar, baking powder, salt, and nutmeg.\nIn another bowl, beat the eggs and then whisk in the milk and vanilla.\nMelt the butter in a large cast iron skillet or griddle over medium heat.\nWhisk the butter into the milk mixture. Add the wet ingredients to the flour mixture, and whisk until a thick batter is just formed.\nKeeping the skillet at medium heat, ladle about 1/4 cup of the batter onto the skillet, to make a pancake. Make 1 or 2 more pancakes, taking care to keep them evenly spaced apart. Cook, until bubbles break the surface of the pancakes, and the undersides are golden brown, about 2 minutes. Flip with a spatula and cook about 1 minute more on the second side. Serve immediately or transfer to a platter and cover loosely with foil to keep warm. Repeat with the remaining batter, adding more butter to the skillet as needed.\nProcedure for adding fruit to pancakes: Once the bubbles break the surface of the pancakes, scatter the surface with sliced or diced fruit, or chocolate chips, nuts, etc. Flip with a spatula and cook for 1 minute more, being careful not to burn toppings. ', 'pancakes.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `clave`) VALUES
(1, 'manolo', '12345'),
(2, 'maria', '12345');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cAjenas` (`id_autor`) USING BTREE;

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `recetas`
--
ALTER TABLE `recetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD CONSTRAINT `recetas_ibfk_2` FOREIGN KEY (`id_autor`) REFERENCES `usuarios` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
