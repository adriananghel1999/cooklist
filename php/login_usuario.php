<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['name']) && isset($_POST['password'])) {
    
    $name = $_POST['name'];
    $password = $_POST['password'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysqli_query($db->connect(), "SELECT * FROM `usuarios` WHERE `nombre`= '$name' and `clave` = '$password'");
    
    $row = mysqli_fetch_array($result);
    
    

    // check if row inserted or not
    if (empty($row)) {
		// failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
        
        // echoing JSON response
        echo json_encode($response);
        
    } else {
		// successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Usuario encontrado.";
        $response["usuario"] = $row["id"];
        

        // echoing JSON response
        echo json_encode($response);
		
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
