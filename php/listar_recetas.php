<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// get all products from products table
$result = mysqli_query($db->connect(), "SELECT * FROM recetas") or die(mysqli_error());

// check for empty result
if (mysqli_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["recetas"] = array();
    
    while ($row = mysqli_fetch_array($result)) {
        // temp user array
        $receta = array();
        $receta["id"] = $row["id"];
        
        $id_autor = $row["id_autor"];
        $categoria_id = $row["categoria_id"];
        
        $receta["id_autor"] = $id_autor;
        $receta["categoria_id"] = $categoria_id;
        
        $autor = mysqli_query($db->connect(), "SELECT `nombre` FROM `usuarios` WHERE `id`= '$id_autor'");
        $categoria = mysqli_query($db->connect(), "SELECT `nombre` FROM `categorias` WHERE `id`= '$categoria_id'");
        
        $fila = mysqli_fetch_array($autor);
        $receta["autor"] = $fila["nombre"];
        $fila = mysqli_fetch_array($categoria);
        $receta["categoria"] = $fila["nombre"];
        
        $receta["nombre"] = $row["nombre"];
        $receta["ingredientes"] = $row["ingredientes"];
        $receta["pasos"] = $row["pasos"];
        $receta["imagen"] = $row["imagen"];

        // push single product into final response array
        array_push($response["recetas"], $receta);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No recetas found";

    // echo no users JSON
    echo json_encode($response);
}
?>
