<?php

/*
 * Following code will update a product information
 * A product is identified by product id (pid)
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['ingredientes']) && isset($_POST['pasos'])) {
    
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $ingredientes = $_POST['ingredientes'];
    $pasos = $_POST['pasos'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql update row with matched pid
    $result = mysqli_query($db->connect(), "UPDATE `aga_cooklist`.`recetas` SET `nombre` = '$nombre', `ingredientes` = '$ingredientes', `pasos` = '$pasos' WHERE `recetas`.`id` = '$id';");

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "receta successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
