package com.example.ruman.cocklist;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    ListView mainListView;
    CheckBox recipeFav;
    ArrayList<Recipe> arrayRecipes;
    MySQLiteHelper db;
    ImageAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes);

        db = new MySQLiteHelper(this);

        recipeFav = (CheckBox) findViewById(R.id.recipeFav);
        mainListView = (ListView) findViewById(R.id.recipeList);

        arrayRecipes = db.getAllRecipes();
        adaptador = new ImageAdapter(this, arrayRecipes);

        mainListView.setAdapter(adaptador);

        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idBBDD = ((TextView) view.findViewById(R.id.idBD)).getText()
                        .toString();

                // Starting new intent
                Intent in = new Intent(getApplicationContext(), RecipeActivity.class);
                // sending pid to next activity
                in.putExtra("id", idBBDD);

                // starting new activity and expecting some response back
                startActivityForResult(in, 100);

            }
        });



    }

    // Respuesta de RecipeActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted product
            // reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

}
