package com.example.ruman.cocklist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OnlineAdd extends AppCompatActivity {

    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    EditText recipeName, recipeIngr, recipeSteps;

    // url to create new product
    private static String url_crear_receta = "/crear_receta.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    int idUsuario;
    int exito = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newrecipe);

        // Edit Text
        recipeName = (EditText) findViewById(R.id.recipeName);
        recipeIngr = (EditText) findViewById(R.id.recipeIngr);
        recipeSteps = (EditText) findViewById(R.id.recipeSteps);

        Intent i = getIntent();

        idUsuario = i.getExtras().getInt("idUsuario");

        // Create button
        Button btnCreateReceta = (Button) findViewById(R.id.button2);

        // button click event
        btnCreateReceta.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread

                if (recipeName.getText().toString().equals("") || recipeIngr.getText().toString().equals("") || recipeSteps.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "No dejes ningun campo vacio", Toast.LENGTH_SHORT).show();
                } else {

                    new CreateNewReceta().execute();
                }
            }
        });
    }

    /**
     * Background Async Task to Create new product
     * */
    class CreateNewReceta extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OnlineAdd.this);
            pDialog.setMessage("Creando receta..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            String nombre = recipeName.getText().toString();
            String ingredientes = recipeIngr.getText().toString();
            String pasos = recipeSteps.getText().toString();
            String idUsuarioString = String.valueOf(idUsuario);

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nombre", nombre));
            params.add(new BasicNameValuePair("ingredientes", ingredientes));
            params.add(new BasicNameValuePair("pasos", pasos));
            params.add(new BasicNameValuePair("idUsuario", idUsuarioString));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_crear_receta,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    exito = 1;

                } else {
                    exito = 0;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

            if (exito == 1){
                // dismiss the dialog once done
                pDialog.dismiss();
                // successfully created product
                Toast.makeText(getApplicationContext(), "Receta creada", Toast.LENGTH_SHORT).show();

                // closing this screen
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }


        }

    }

    }

