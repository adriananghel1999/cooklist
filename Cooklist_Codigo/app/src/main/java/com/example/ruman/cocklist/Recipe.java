package com.example.ruman.cocklist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.Serializable;

public class Recipe implements Serializable {

    private int id;
    private String name;
    private String ingredients;
    private String steps;
    private int favorite; // 0 or 1
    private String image;

    public Recipe(){}

    public Recipe(String name, String ingredients, String steps, int favorite, String image) {
        super();
        this.name = name;
        this.ingredients = ingredients;
        this.steps = steps;
        this.favorite = favorite;
        this.image = image;
    }

    @Override
    public String toString(){
        return "id= "+ id +"name=" + name + " ingredients=" + ingredients + " steps=" + steps + " favorite=" + String.valueOf(favorite) + " image=" + image;
    }

    //getters & setters

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getIngredients(){
        return ingredients;
    }

    public String getSteps(){
        return steps;
    }

    public int getFavorite(){
        return favorite;
    }

    public String getImage(){
        return image;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setIngredients(String ingredients){
        this.ingredients = ingredients;
    }

    public void setSteps(String steps){
        this.steps = steps;
    }

    public void setFavorite(int favorite){
        this.favorite = favorite;
    }

    public void setImage(String image){
        this.image = image;
    }

}
