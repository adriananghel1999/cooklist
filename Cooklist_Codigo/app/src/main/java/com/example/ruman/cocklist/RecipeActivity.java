package com.example.ruman.cocklist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.IOException;

import javax.microedition.khronos.egl.EGLDisplay;

import static com.example.ruman.cocklist.NewRecipeActivity.PICK_IMAGE_REQUEST;

public class RecipeActivity extends AppCompatActivity {

    String idBD;
    CheckBox checkBox;
    Button button;
    Button guardar;
    EditText recipeName;
    EditText recipeIngr;
    EditText recipeSteps;
    MySQLiteHelper db;
    Recipe recipe;
    Intent intent;
    ImageView image;
    public static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        checkBox = (CheckBox) findViewById(R.id.checkBox);
        guardar = (Button) findViewById(R.id.guardar);
        button = (Button) findViewById(R.id.button);

        db = new MySQLiteHelper(this);

        Intent i = getIntent();

        idBD = i.getExtras().getString("id");

        recipe = db.getRecipe(Integer.parseInt(idBD));

        // Imagen
        ImageView picture = (ImageView) findViewById(R.id.imageView2);

        int resId = getApplicationContext().getResources().getIdentifier(recipe.getImage(),"drawable",getApplicationContext().getPackageName());
        Drawable d = getApplicationContext().getResources().getDrawable(resId);

        picture.setImageDrawable(d);

        // Nombre
        recipeName = (EditText) findViewById(R.id.recipeName);
        recipeName.setText(recipe.getName());

        // Favorito
        if (recipe.getFavorite() == 1){
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        recipeIngr = (EditText) findViewById(R.id.recipeIngr);
        recipeIngr.setText(recipe.getIngredients());

        recipeSteps = (EditText) findViewById(R.id.recipeSteps);
        recipeSteps.setText(recipe.getSteps());

        image = findViewById(R.id.imageView2);

        // Boton guardar
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkBox.isChecked()){
                    recipe.setFavorite(1);
                } else {
                    recipe.setFavorite(0);
                }

                recipe.setName(recipeName.getText().toString());
                //recipe.setImage();
                recipe.setSteps(recipeSteps.getText().toString());
                recipe.setIngredients(recipeIngr.getText().toString());

                db.updateRecipe(recipe);

                // notify previous activity by sending code 100
                Intent i = getIntent();
                // send result code 100 to notify about product deletion
                setResult(100, i);
                finish();

            }
        });

        // Boton borrar
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(RecipeActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Are you sure?");
                builder.setMessage("Do you want to delete this recipe?");
                builder.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                db.deleteRecipe(recipe);

                                // notify previous activity by sending code 100
                                Intent i = getIntent();
                                // send result code 100 to notify about product deletion
                                setResult(100, i);
                                finish();
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
