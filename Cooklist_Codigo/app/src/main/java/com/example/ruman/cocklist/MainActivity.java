package com.example.ruman.cocklist;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    MySQLiteHelper db;
    MediaPlayer cruch;
    CheckBox recipeFav;

    EditText etLoginName, etLoginPassword, etRegisterName, etRegisterPassword;
    Button btnLogin, btnRegister;

    // Online

    private static String url_login = "/login_usuario.php";
    private static String url_register = "/registrar_usuario.php";

    private static final String TAG_SUCCESS = "success";
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    int idUsuario;

    private int exito = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        db = new MySQLiteHelper(this);

        // Example Recipes
/*

        db.addRecipe(new Recipe("Tofu and Veggies in Peanut Sauce", "    1 tablespoon peanut oil\n" +
                "    1 small head broccoli, chopped\n" +
                "    1 small red bell pepper, chopped\n" +
                "    5 fresh mushrooms, sliced\n" +
                "    1 pound firm tofu, cubed\n" +
                "    1/2 cup peanut butter\n" +
                "    1/2 cup hot water\n" +
                "    2 tablespoons vinegar\n" +
                "    2 tablespoons soy sauce\n" +
                "    1 1/2 tablespoons molasses\n" +
                "    ground cayenne pepper to taste", "\n" +
                "    Heat oil in a large skillet or wok over medium-high heat. Saute broccoli, red bell pepper, mushrooms and tofu for 5 minutes.\n" +
                "    In a small bowl combine peanut butter, hot water, vinegar, soy sauce, molasses and cayenne pepper. Pour over vegetables and tofu. Simmer for 3 to 5 minutes, or until vegetables are tender crisp.\n", 0, "tofu_and_veggies_in_peanut_sauce"));

        db.addRecipe(new Recipe("Hamburger Steak with Onions and Gravy", "    1 pound ground beef\n" +
                "    1 egg\n" +
                "    1/4 cup bread crumbs\n" +
                "    1/8 teaspoon ground black pepper\n" +
                "    1/2 teaspoon seasoned salt\n" +
                "    1/2 teaspoon onion powder\n" +
                "    1/2 teaspoon garlic powder\n" +
                "    1 teaspoon Worcestershire sauce\n" +
                "    1 tablespoon vegetable oil\n" +
                "    1 cup thinly sliced onion\n" +
                "    2 tablespoons all-purpose flour\n" +
                "    1 cup beef broth\n" +
                "    1 tablespoon cooking sherry\n" +
                "    1/2 teaspoon seasoned salt", "\n" +
                "    In a large bowl, mix together the ground beef, egg, bread crumbs, pepper, salt, onion powder, garlic powder, and Worcestershire sauce. Form into 8 balls, and flatten into patties.\n" +
                "    Heat the oil in a large skillet over medium heat. Fry the patties and onion in the oil until patties are nicely browned, about 4 minutes per side. Remove the beef patties to a plate, and keep warm.\n" +
                "    Sprinkle flour over the onions and drippings in the skillet. Stir in flour with a fork, scraping bits of beef off of the bottom as you stir. Gradually mix in the beef broth and sherry. Season with seasoned salt. Simmer and stir over medium-low heat for about 5 minutes, until the gravy thickens. Turn heat to low, return patties to the gravy, cover, and simmer for another 15 minutes.\n", 0, "hamburger_steak_with_onions_and_gravy"));

        db.addRecipe(new Recipe("Green Risotto with Fava Beans", "    1/2 pound fresh, unshelled fava beans\n" +
                "    4 cups chicken broth\n" +
                "    3 tablespoons butter, divided\n" +
                "    1 small onion, finely chopped\n" +
                "    1 cup Arborio rice\n" +
                "    1/4 cup white wine\n" +
                "    1/4 cup grated Reggiano Parmesan cheese\n" +
                "    salt to taste", "\n" +
                "    Bring a large pot of salted water to a boil. Meanwhile, shell the favas and discard the pods. Boil the favas for 4 minutes, strain and then immediately plunge into ice water. Let cool for 2 minutes then pierce the favas and squeeze them out of their skins. Separate 3/4 of the favas and puree in a food processor.\n" +
                "    In a separate large saucepan bring the broth to a simmer, and keep it hot. Meanwhile, in another large saucepan over medium heat, melt 1.5 tablespoons of the butter and add the onions. Reduce the heat to low and cook for about 5 minutes; do not brown the onions. Add the rice and cook, while stirring, for 2 minutes. Add the wine, increase the heat to medium, and stir constantly. When the wine has been absorbed, add a little of the hot stock. Once the stock is absorbed, add a little more; repeat this process, stirring constantly, until the rice is cooked through.\n" +
                "    To the cooked rice add the pureed favas, the remaining 1.5 tablespoons of butter, the rest of the favas and the cheese. Cook over medium heat, stirring, until the butter and cheese melt and the puree is incorporated evenly. Season with salt.\n", 0, "green_risotto_with_fava_beans"));

        db.addRecipe(new Recipe("Creamy Au Gratin Potatoes", "    4 russet potatoes, sliced into 1/4 inch slices\n" +
                "    1 onion, sliced into rings\n" +
                "    salt and pepper to taste\n" +
                "    3 tablespoons butter\n" +
                "    3 tablespoons all-purpose flour\n" +
                "    1/2 teaspoon salt\n" +
                "    2 cups milk\n" +
                "    1 1/2 cups shredded Cheddar cheese", "\n" +
                "    Preheat oven to 400 degrees F (200 degrees C). Butter a 1 quart casserole dish.\n" +
                "    Layer 1/2 of the potatoes into bottom of the prepared casserole dish. Top with the onion slices, and add the remaining potatoes. Season with salt and pepper to taste.\n" +
                "    In a medium-size saucepan, melt butter over medium heat. Mix in the flour and salt, and stir constantly with a whisk for one minute. Stir in milk. Cook until mixture has thickened. Stir in cheese all at once, and continue stirring until melted, about 30 to 60 seconds. Pour cheese over the potatoes, and cover the dish with aluminum foil.\n" +
                "    Bake 1 1/2 hours in the preheated oven.\n", 0, "creamy_au_gratin_potatoes"));

        db.addRecipe(new Recipe("French Toast", "    1 teaspoon ground cinnamon\n" +
                "    1/4 teaspoon ground nutmeg\n" +
                "    2 tablespoons sugar\n" +
                "    4 tablespoons butter\n" +
                "    4 eggs\n" +
                "    1/4 cup milk\n" +
                "    1/2 teaspoon vanilla extract\n" +
                "    8 slices challah, brioche, or white bread\n" +
                "    1/2 cup maple syrup, warmed", "\n" +
                "\n" +
                "In a small bowl, combine, cinnamon, nutmeg, and sugar and set aside briefly.\n" +
                "\n" +
                "In a 10-inch or 12-inch skillet, melt butter over medium heat. Whisk together cinnamon mixture, eggs, milk, and vanilla and pour into a shallow container such as a pie plate. Dip bread in egg mixture. Fry slices until golden brown, then flip to cook the other side. Serve with syrup.\n", 0, "french_toast"));
*/


        cruch = MediaPlayer.create(this, R.raw.cruch);

        // PARTE ONLINE

        etLoginName = findViewById(R.id.etLoginName);
        etLoginPassword = findViewById(R.id.etLoginPassword);
        etRegisterName = findViewById(R.id.etRegisterName);
        etRegisterPassword = findViewById(R.id.etRegisterPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etLoginName.getText().toString().equals("") || etLoginPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "No dejes ningun campo vacio", Toast.LENGTH_SHORT).show();
                } else {

                    new LoginUser().execute();

                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etRegisterName.getText().toString().equals("") || etRegisterPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "No dejes ningun campo vacio", Toast.LENGTH_SHORT).show();
                } else {
                    new CreateUser().execute();
                }
            }
        });

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            cruch.start();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Cocklist");
            alertDialogBuilder.setMessage("Application that serves to view and add recipes to your device. Made as a project by me, Adrian George Anghel, as a proyect to learn about external and internal databases in android.");
            alertDialogBuilder.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_search) {
            cruch.start();
            Intent i = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_newrecipe) {
            cruch.start();
            Intent i = new Intent(getApplicationContext(), NewRecipeActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static int getRandom(int[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    class CreateUser extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Creating User..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            String name = etRegisterName.getText().toString();
            String password = etRegisterPassword.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("password", password));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_register,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {



                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Usuario creado correctamente", Toast.LENGTH_SHORT).show();
            etRegisterName.setText("");
            etRegisterPassword.setText("");
        }

    }

    class LoginUser extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Login in..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            String name = etLoginName.getText().toString();
            String password = etLoginPassword.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("password", password));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_login,
                    "POST", params);

            // check log cat fro response
            Log.i("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    idUsuario = json.getInt("usuario");

                exito = 1;

                } else {
                    exito = 0;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();

            if (exito == 1){

                Intent i = new Intent(getApplicationContext(), MainOnline.class);
                i.putExtra("idUsuario", idUsuario);
                startActivity(i);


            } else {
                Toast.makeText(getApplicationContext(), "Datos erroneos", Toast.LENGTH_SHORT).show();
            }

        }

    }



}
