package com.example.ruman.cocklist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OnlineEdit extends AppCompatActivity {

    CheckBox checkBox;
    EditText recipeName, recipeIngr, recipeSteps;
    Button guardar, borrar;

    String idUsuario;
    String id;

    MySQLiteHelper db;

    // RECETA

    JSONObject receta;

    // Progress Dialog
    private ProgressDialog pDialog;

    // JSON parser class
    JSONParser jsonParser = new JSONParser();

    // single product url
    private static final String url_una_receta = "/una_receta.php";

    // url to update product
    private static final String url_update_receta = "/editar_receta.php";

    // url to delete product
    private static final String url_delete_receta = "/borrar_receta.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .build());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        recipeName = findViewById(R.id.recipeName);
        recipeIngr = findViewById(R.id.recipeIngr);
        recipeSteps = findViewById(R.id.recipeSteps);

        // save button
        guardar = (Button) findViewById(R.id.guardar);
        borrar = (Button) findViewById(R.id.button);
        checkBox = findViewById(R.id.checkBox);

        checkBox.setText("Local");

        db = new MySQLiteHelper(this);

        // getting product details from intent
        Intent i = getIntent();

        // getting product id (pid) from intent
        id = i.getStringExtra("id");
        idUsuario = i.getStringExtra("idUsuario");

        // Getting complete product details in background thread
        new GetReceta().execute();

        // save button click event
        guardar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // starting background task to update product
                new GuardarReceta().execute();
            }
        });

        // Delete button click event
        borrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // deleting product in background thread
                new BorrarReceta().execute();
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Receta guardada en local", Toast.LENGTH_SHORT).show();

                db.addRecipe(new Recipe(recipeName.getText().toString(), recipeIngr.getText().toString(), recipeSteps.getText().toString(), 0, "clickme"));

            }
        });

    }

    /**
     * Background Async Task to Get complete product details
     * */
    class GetReceta extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OnlineEdit.this);
            pDialog.setMessage("Loading receta details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Getting product details in background thread
         * */
        protected String doInBackground(String... args) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    // Check for success tag
                    int success;
                    try {
                        // Building Parameters
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", id));

                        // getting product details by making HTTP request
                        // Note that product details url will use GET request
                        JSONObject json = jsonParser.makeHttpRequest(
                                url_una_receta, "GET", params);

                        // check your log for json response
                        Log.d("Single receta Details", json.toString());

                        // json success tag
                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            // successfully received product details
                            JSONArray productObj = json
                                    .getJSONArray("receta"); // JSON Array

                            // get first product object from JSON Array
                            receta = productObj.getJSONObject(0);



                        }else{
                            // product with pid not found
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once got all details
            pDialog.dismiss();

            // display product data in EditText
            try {
                recipeName.setText(receta.getString("nombre"));
                recipeIngr.setText(receta.getString("ingredientes"));
                recipeSteps.setText(receta.getString("pasos"));

                // Comprobaciones para el autor

                if(idUsuario.equals(receta.getString("id_autor"))){
                    guardar.setEnabled(true);
                    borrar.setEnabled(true);
                    recipeName.setEnabled(true);
                    recipeIngr.setEnabled(true);
                    recipeSteps.setEnabled(true);
                } else {
                    guardar.setEnabled(false);
                    borrar.setEnabled(false);
                    recipeName.setEnabled(false);
                    recipeIngr.setEnabled(false);
                    recipeSteps.setEnabled(false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Background Async Task to  Save product Details
     * */
    class GuardarReceta extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OnlineEdit.this);
            pDialog.setMessage("Saving receta ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Saving product
         * */
        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String nombre = recipeName.getText().toString();
            String ingredientes = recipeIngr.getText().toString();
            String pasos = recipeSteps.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id", id));
            params.add(new BasicNameValuePair("nombre", nombre));
            params.add(new BasicNameValuePair("ingredientes", ingredientes));
            params.add(new BasicNameValuePair("pasos", pasos));

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_update_receta,
                    "POST", params);

            // check json success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product uupdated
            pDialog.dismiss();
        }
    }

    /*****************************************************************
     * Background Async Task to Delete Product
     * */
    class BorrarReceta extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OnlineEdit.this);
            pDialog.setMessage("Deleting receta...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Deleting product
         * */
        protected String doInBackground(String... args) {

            // Check for success tag
            try {
                // Building Parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("id", id));

                // getting product details by making HTTP request
                JSONObject json = jsonParser.makeHttpRequest(
                        url_delete_receta, "POST", params);

                // check your log for json response
                Log.d("Delete Product", json.toString());

                // json success tag
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // product successfully deleted
                    // notify previous activity by sending code 100
                    Intent i = getIntent();
                    // send result code 100 to notify about product deletion
                    setResult(100, i);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();

        }

    }
}

