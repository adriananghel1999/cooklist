package com.example.ruman.cocklist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MySQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "RecipeDB";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_RECIPES_TABLE = "CREATE TABLE recipes ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, "+
                "ingredients TEXT, "+
                "steps TEXT, "+
                "favorite INTEGER, "+
                "image TEXT )";

        // create books table
        db.execSQL(CREATE_RECIPES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS recipes");

        // create fresh books table
        this.onCreate(db);
    }
    //---------------------------------------------------------------------

    // Books table name
    private static final String TABLE_RECIPES = "recipes";

    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_INGREDIENTS = "ingredients";
    private static final String KEY_STEPS = "steps";
    private static final String KEY_FAVORITE = "favorite";
    private static final String KEY_IMAGE = "image";

    private static final String[] COLUMNS = {KEY_ID,KEY_NAME,KEY_INGREDIENTS,KEY_STEPS,KEY_FAVORITE,KEY_IMAGE};

    public void addRecipe(Recipe recipe){
        //Log.i("addRecipe", recipe.toString());

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, recipe.getName()); // get title
        values.put(KEY_INGREDIENTS, recipe.getIngredients());
        values.put(KEY_STEPS, recipe.getSteps());
        values.put(KEY_FAVORITE, recipe.getFavorite());
        values.put(KEY_IMAGE, recipe.getImage());

        // 3. insert
        db.insert(TABLE_RECIPES, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public Recipe getRecipe(int id){

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(TABLE_RECIPES, // a. table
                        COLUMNS, // b. column names
                        " id = ?", // c. selections
                        new String[] { String.valueOf(id) }, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        if (cursor != null)
            cursor.moveToFirst();

        // 4. build book object
        Recipe recipe = new Recipe();
        recipe.setId(Integer.parseInt(cursor.getString(0)));
        recipe.setName(cursor.getString(1));
        recipe.setIngredients(cursor.getString(2));
        recipe.setSteps(cursor.getString(3));
        recipe.setFavorite(Integer.parseInt(cursor.getString(4)));
        recipe.setImage(cursor.getString(5));

        Log.d("getRecipe("+id+")", recipe.toString());

        // 5. return book
        return recipe;
    }

    // Get All Books
    public ArrayList<Recipe> getAllRecipes() {
        ArrayList<Recipe> recipes = new ArrayList<Recipe>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_RECIPES;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Recipe recipe = null;
        if (cursor.moveToFirst()) {
            do {
                recipe = new Recipe();
                recipe.setId(Integer.parseInt(cursor.getString(0)));
                recipe.setName(cursor.getString(1));
                recipe.setIngredients(cursor.getString(2));
                recipe.setSteps(cursor.getString(3));
                recipe.setFavorite(Integer.parseInt(cursor.getString(4)));
                recipe.setImage(cursor.getString(5));

                // Add book to books
                recipes.add(recipe);
            } while (cursor.moveToNext());
        }

        // return books
        return recipes;
    }

    // Updating single book
    public int updateRecipe(Recipe recipe) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("name", recipe.getName()); // get title
        values.put("ingredients", recipe.getIngredients());
        values.put("steps", recipe.getSteps());
        values.put("favorite", recipe.getFavorite());
        values.put("image", recipe.getImage());

        // 3. updating row
        int i = db.update(TABLE_RECIPES, //table
                values, // column/value
                KEY_ID+" = ?", // selections
                new String[] { String.valueOf(recipe.getId()) }); //selection args

        // 4. close
        db.close();

        return i;

    }

    // Deleting single book
    public void deleteRecipe(Recipe recipe) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. delete
        db.delete(TABLE_RECIPES,
                KEY_ID+" = ?",
                new String[] { String.valueOf(recipe.getId()) });

        // 3. close
        db.close();

    }
}
