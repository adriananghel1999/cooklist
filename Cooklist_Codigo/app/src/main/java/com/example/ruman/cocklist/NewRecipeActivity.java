package com.example.ruman.cocklist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class NewRecipeActivity extends AppCompatActivity {

    public static final int PICK_IMAGE_REQUEST = 1;
    Button finish;
    EditText recipeName, recipeIngr, recipeSteps;
    ImageButton image;
    Intent intent;
    MySQLiteHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newrecipe);

        db = new MySQLiteHelper(this);

        finish = (Button) findViewById(R.id.button2);
        recipeName = (EditText) findViewById(R.id.recipeName);
        recipeIngr = (EditText) findViewById(R.id.recipeIngr);
        recipeSteps = (EditText) findViewById(R.id.recipeSteps);
        image = (ImageButton) findViewById(R.id.imageButton);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

            }


        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (recipeName.getText().toString().equals("") || recipeIngr.getText().toString().equals("") || recipeSteps.getText().toString().equals("") ){
                    Toast.makeText(getApplicationContext(), "No dejes ningun campo vacio", Toast.LENGTH_SHORT).show();
                } else {

                    db.addRecipe(new Recipe(recipeName.getText().toString(), recipeIngr.getText().toString(), recipeSteps.getText().toString(), 0, "clickme"));
                    finish();
                }



            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
