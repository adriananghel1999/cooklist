package com.example.ruman.cocklist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainOnline extends AppCompatActivity {

    Button btnView, btnAdd;
    int idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_online);

        btnView = findViewById(R.id.btnView);
        btnAdd = findViewById(R.id.btnAdd);

        Intent i = getIntent();

        idUsuario = i.getExtras().getInt("idUsuario");

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), OnlineAll.class);
                i.putExtra("idUsuario", idUsuario);
                startActivity(i);

            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), OnlineAdd.class);
                i.putExtra("idUsuario", idUsuario);
                startActivity(i);
            }
        });
    }
}
