package com.example.ruman.cocklist;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ImageAdapter extends BaseAdapter{

    private final Activity act;
    public ArrayList<Recipe> RecipeArray;

    public ImageAdapter(Activity act, ArrayList<Recipe> recipes){
        this.act = act;
        this.RecipeArray = recipes;

    }

    public void changeArray(ArrayList<Recipe> recipes){
        this.RecipeArray = recipes;
    }

    @Override
    public int getCount(){
        return RecipeArray.size();
    }

    @Override
    public Object getItem(int position) {
        return RecipeArray.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = act.getLayoutInflater();
        View layview = inflater.inflate(R.layout.activity_recipe_in_list, null);

        ImageView imagen = (ImageView) layview.findViewById(R.id.imageView3);
        //RecipeArray.get(position).getImage()
        int resId = act.getResources().getIdentifier(RecipeArray.get(position).getImage(),"drawable",act.getPackageName());
        Drawable d = act.getResources().getDrawable(resId);

        imagen.setImageDrawable(d);

        TextView idBD = layview.findViewById(R.id.idBD);
        idBD.setText(String.valueOf(RecipeArray.get(position).getId()));

        TextView recipe = (TextView) layview.findViewById(R.id.recipeName);
        recipe.setText(RecipeArray.get(position).getName());

        CheckBox favorite = (CheckBox) layview.findViewById(R.id.recipeFav);
        if (RecipeArray.get(position).getFavorite() == 0){
            favorite.setChecked(false);
        } else {
            favorite.setChecked(true);

        }
        return layview;
    }




}